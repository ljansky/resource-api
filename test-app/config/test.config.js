const mockAdapter = require('db-storage').mockAdapter;
const getStorageFactory = require('db-storage').getStorageFactory;
const loadStorageConfigs = require('db-storage').loadStorageConfigs;
const entityFormatter = require('entity-formatter');
const getResourceFactory = require('../../lib/resource').getResourceFactory;
const getCoreModelsPath = require('../../lib').getCoreModelsPath;
const api = require('../api');

module.exports = async (getStorage, getResource) => {
	return {
		services: {
			getStorage,
			getResource
		},
		api
	};
};