const KoaRouter = require('koa-router');
const router = new KoaRouter();

const servicesApi = require('./services-api');

router.use('/services', servicesApi.routes());

module.exports = router;