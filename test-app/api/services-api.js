const KoaRouter = require('koa-router');
const R = require('ramda');
const router = new KoaRouter();

router.get('/', async function (ctx) {
	ctx.body = R.keys(ctx.services);
});

module.exports = router;