const chai = require('chai');
const assert = chai.assert;
const getPrefixObject = require('../lib/getPrefixObject');

describe('GetPrefixObject', () => {
	it('should get subObject', () => {
		const inputObject = {
			'f.s1': 'test1',
			'notF.s3': 'test3',
			'f.s2': 'test2',
			test: 'test'
		};

		const expected = {
			s1: inputObject['f.s1'],
			s2: inputObject['f.s2']
		};

		const subObject = getPrefixObject('f')(inputObject);

		assert.deepEqual(subObject, expected);
	});
});