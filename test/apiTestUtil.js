const R = require('ramda');
const supertestRequest = require('supertest');
const acl = require('../lib/acl');

let userCounter = 0;

const login = (request, config) => async (roles = []) => {

	const userResource = config.services.getResource('user', [], {
		onInsertData: acl.getOnInsertData(config.services.getResource, null)
	});

	userCounter++;
	const user = await userResource.insertOne({
		email: `test${ userCounter }@test.cz`,
		loginPassword: [{
			password: `password${ userCounter }`
		}],
		userRole: roles.map(roleId => ({ role_id: roleId }))
	});

	const credentials = {
		email: user.email,
		password: user.loginPassword[0] ? user.loginPassword[0].password : ''
	}

	const userInfo = await post(request)('/api/auth/login', 200, null, credentials);

	const publicAcl = R.find(R.whereEq({ system: 1, title: 'public' }))(user.acl);
	const privateAcl = R.find(R.whereEq({ system: 1, title: 'private' }))(user.acl);

	return R.merge(user, {
		token: userInfo.token,
		publicAcl,
		privateAcl
	});
}

const get = request => (uri, expect, token, query) => {
	return request
		.get(uri)
		.set('Authorization', 'Bearer ' + token)
		.query(query)
		.expect(expect)
		.then(res => res.body);
};

const post = request => (uri, expect, token, data, query) => {
	return request
		.post(uri)
		.set('Authorization', 'Bearer ' + token)
		.query(query)
		.send(data)
		.expect(expect)
		.then(res => res.body);
};

const put = request => (uri, expect, token, data, query) => {
	return request
		.put(uri)
		.set('Authorization', 'Bearer ' + token)
		.query(query)
		.send(data)
		.expect(expect)
		.then(res => res.body);
};

const del = request => (uri, expect, token, query) => {
	return request
		.delete(uri)
		.set('Authorization', 'Bearer ' + token)
		.query(query)
		.expect(expect)
		.then(res => res.body);
};

const getApiTestUtil = (app, config) => {

	const request = supertestRequest(app.listen());

	return {
		get: get(request),
		post: post(request),
		put: put(request),
		delete: del(request),
		login: roles => login(request, config)(roles)
	};
};

module.exports = {
	getApiTestUtil
};
