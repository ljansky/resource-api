const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const getResourceFactory = require('../lib/resource').getResourceFactory;
const getStorageFactory = require('db-storage').getStorageFactory;
const loadStorageConfigs = require('db-storage').loadStorageConfigs;
const mockAdapter = require('db-storage').mockAdapter;
const entityFormatter = require('entity-formatter');
const dbTestConnection = require('./dbTestConnection');

describe('Resource service', () => {

	beforeEach(async function () {
		await dbTestConnection.start(this);
		this.testResource = this.getResource('testEntity');

		this.entity = await this.testResource.insertOne({
			title: 'test',
			test_number: 1,
			testEntityRelated: [{}, {}]
		});

		assert.equal(this.entity.test_number, 1);
		assert.isDefined(this.entity.id);
	});

	afterEach (async function () {
		await dbTestConnection.stop(this);
	});

	it('should find formatted entity', async function () {
		const expected = { id: this.entity.id };
		const foundEntity = await this.testResource.findOneById(this.entity.id, 'id');
		assert.deepEqual(foundEntity, expected);
	});

	it('should find formatted entity directly by format object', async function () {
		const expected = { id: this.entity.id };
		const foundEntity = await this.testResource.findOneById(this.entity.id, { id: 1 });
		assert.deepEqual(foundEntity, expected);
	});

	it('should find and format more entities', async function () {
		const entity2 = await this.testResource.insertOne({
			test_number: 2
		});

		const expected = [{
			test_number: this.entity.test_number
		}, {
			test_number: entity2.test_number
		}];

		const foundEntities = await this.testResource.findAll({ format: 'test_number' });
		assert.deepEqual(foundEntities, expected);
	});

	it('should find with related entities', async function () {
		const expected = {
			id: this.entity.id,
			testEntityRelated: [{
				test_entity_id: this.entity.id
			}, {
				test_entity_id: this.entity.id
			}]
		};

		const foundEntity = await this.testResource.findOneById(this.entity.id, 'id,testEntityRelated.test_entity_id');
		assert.deepEqual(foundEntity, expected); 
	});

	it('should find entities with limit', async function () {
		const relatedResource = this.getResource('testEntityRelated');
		const data = await relatedResource.findAll({ limit: 1 });

		assert.deepEqual(data, [this.entity.testEntityRelated[0]]);
	});

	it('should find entities with offset', async function () {
		const relatedResource = this.getResource('testEntityRelated');
		const data = await relatedResource.findAll({ offset: 1 });

		assert.deepEqual(data, [this.entity.testEntityRelated[1]]);
	});

	it('should find entities ordered by id desc', async function () {
		const order = [{
			by: 'id',
			desc: true
		}];

		const relatedResource = this.getResource('testEntityRelated');
		const data = await relatedResource.findAll({ order });

		assert.deepEqual(data, [this.entity.testEntityRelated[1], this.entity.testEntityRelated[0]]);
	});

	it.skip('should not be possible to use undefined models', {});

	it('should be possible to update existing entity', async function () {

		const expected = R.pipe(
			R.omit(['testEntityRelated']),
			R.assoc('test_number', 2)
		)(this.entity);

		const updatedEntity = await this.testResource.updateOneById(this.entity.id, {
			test_number: 2
		});

		assert.deepEqual(updatedEntity, expected);

		const foundEntity = await this.testResource.findOneById(this.entity.id);
		assert.deepEqual(foundEntity, expected);
	});

	it('should be possible to delete entity', async function () {

		const insertedEntity = await this.testResource.insertOne({
			title: 'test',
			test_number: 1
		});

		const deletedEntity = await this.testResource.deleteOneById(insertedEntity.id);
		assert.deepEqual(deletedEntity, insertedEntity);

		const foundEntity = await this.testResource.findOneById(insertedEntity.id, 'id,test_number,title');
		assert.equal(foundEntity, null);

		const updatedEntity = await this.testResource.updateOneById(insertedEntity.id, {
			test_number: 2
		});

		assert.equal(updatedEntity, null);
	});
});