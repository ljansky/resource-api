const chai = require('chai');
const assert = chai.assert;
const getFilterRelations = require('../lib/getFilterArray');

describe('GetFilterArray', () => {
	it('should get equal filters', () => {
		const query = {
			'f.test1': 'test',
			'f.test2': '1'
		};

		const expected = [{
			field: 'test1',
			operator: 'eq',
			value: 'test'
		}, {
			field: 'test2',
			operator: 'eq',
			value: '1'
		}]

		const result = getFilterRelations(query);
		assert.deepEqual(result, expected);
	})

	it('should get like filter', () => {
		const query = {
			'f.test': 'like(stringValue)'
		};

		const expected = [{
			field: 'test',
			operator: 'like',
			value: 'stringValue'
		}]

		const result = getFilterRelations(query);
		assert.deepEqual(result, expected);
	})
});