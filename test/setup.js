const dbTestConnection = require('./dbTestConnection');

before(async function () {
	await dbTestConnection.init(this);
});