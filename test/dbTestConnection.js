const getResourceFactory = require('../lib/resource').getResourceFactory;
const getStorageFactory = require('db-storage').getStorageFactory;
const loadStorageConfigs = require('db-storage').loadStorageConfigs;
const loadXmlConfigsFromPaths = require('db-storage').loadXmlConfigsFromPaths;
const mockAdapter = require('db-storage').mockAdapter;
const sqlAdapter = require('db-storage-sql');
const dbTest = require('db-test');
const entityFormatter = require('entity-formatter');
const getCoreModelsPath = require('../lib').getCoreModelsPath;
const path = require('path');

const dbAdapter = process.env.DB_ADAPTER || 'mock';

const connectionInfo = {
	host: 'localhost',
	user: 'root',
	password: 'password',
	database: 'db',
	debug: false
};

const init = async (ctx) => {
	if (dbAdapter === 'sql') {
		ctx.timeout(20000);
		const initialized = await dbTest.init(path.join(__dirname, '..', '..', 'db'), connectionInfo, false);
	}
}

const start = async (ctx) => {

	const paths = [
		getCoreModelsPath(),
		__dirname + '/../test-app/models'
	];

	const fixturePaths = [
		__dirname + '/../test-app/fixtures'
	];

	const storageConfigs = await loadStorageConfigs(paths);
	const fixtures = await loadXmlConfigsFromPaths(fixturePaths);

	switch (dbAdapter) {
		case 'sql':
			ctx.timeout(20000);
			ctx.container = await dbTest.start();
			ctx.connection = sqlAdapter.getDbConnection(connectionInfo);
			const sqlAdapterInstance = sqlAdapter.createAdapter(ctx.connection);
			ctx.getStorage = await getStorageFactory(storageConfigs, fixtures, sqlAdapterInstance);
			break;
		default:
			const mockAdapterInstance = mockAdapter.createAdapter({});
			ctx.getStorage = await getStorageFactory(storageConfigs, fixtures, mockAdapterInstance);
			break;
	}

	ctx.getResource = getResourceFactory(ctx.getStorage, entityFormatter);
}

const stop = async (ctx) => {
	if (dbAdapter === 'sql') {
		await dbTest.remove(ctx.container);
	}
}

module.exports = {
	init,
	start,
	stop
};