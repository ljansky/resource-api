const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const getApiTestUtil = require('../apiTestUtil').getApiTestUtil;
const appFactory = require('../../lib/app-factory');
const getTestConfig = require('../../test-app/config/test.config');
const dbTestConnection = require('../dbTestConnection');

describe('Auth api routes', () => {

	const email = 'test@test.cz';
	const password = 'the-password';

	beforeEach(async function () {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);
		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app);

		const userResource = testConfig.services.getResource('user');
		this.user = await userResource.insertOne({
			email,
			loginPassword: [{
				password
			}],
			userRole: [{ role_id: 1 }]
		});
	});

	afterEach (async function () {
		await dbTestConnection.stop(this);
	});

	it('should be forbidden to access protected api without access token', async function () {
		const data = await this.api.get('/api/resource/test', 401);
	})

	it('should not be possible to get access token with wrong password', async function () {
		const userInfo = await this.api.post('/api/auth/login', 401, null, { email,	password: password + 'wrong' });
	});

	it('should not be possible to get access token with wrong email', async function () {
		const userInfo = await this.api.post('/api/auth/login', 401, null, { email: 'wrong' + email, password });
	});

	it('should not be possible to get access token without credentials', async function () {
		const userInfo = await this.api.post('/api/auth/login', 401);
	});

	it('should get working access token with correct email and password', async function () {
		const userInfo = await this.api.post('/api/auth/login', 200, null, {
			email: 'test@test.cz',
			password: 'the-password'
		});

		const token = userInfo.token;
		const data = await this.api.get('/api/resource/testEntity', 200, token);
	});
});