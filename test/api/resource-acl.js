const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const getApiTestUtil = require('../apiTestUtil').getApiTestUtil;
const appFactory = require('../../lib/app-factory');
const getTestConfig = require('../../test-app/config/test.config');
const dbTestConnection = require('../dbTestConnection');

describe('Resource api ACL', () => {
	
	beforeEach(async function () {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);

		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app, testConfig);

		this.user1 = await this.api.login([1]);
		this.user2 = await this.api.login([2]);
		this.user3 = await this.api.login([1, 2]);

		this.inserted = await this.api.post('/api/resource/testEntity', 200, this.user1.token, {
			title: 'test1',
			testEntityRelated: [{}, {}, {}]
		});
	});

	afterEach (async function () {
		await dbTestConnection.stop(this);
	});

	it('Should read only resource which is in users role permissions', async function () {
		await this.api.get('/api/resource/testEntity2', 200, this.user1.token);
		await this.api.get('/api/resource/testEntity2', 403, this.user2.token);
		await this.api.get('/api/resource/testEntity2', 200, this.user3.token);
	});

	it('Should be possible to create entity only with user role permissions', async function () {
		await this.api.post('/api/resource/testEntity2', 200, this.user1.token, {});
		await this.api.get('/api/resource/testEntity2', 403, this.user2.token, {});
		await this.api.get('/api/resource/testEntity2', 200, this.user3.token, {});
	});

	it('Should not be possible to create acl by resource api', async function () {
		const insertedAcl = await this.api.post('/api/resource/acl', 403, this.user1.token, {
			title: 'ggg',
			member_read: 1
		});
	});

	it('Should not be possible to update acl by resource api', async function () {
		const updatedAcl = await this.api.put('/api/resource/acl/' + this.user1.publicAcl.id, 403, this.user1.token, { title: 'ggg1' });
		const aclUser = await this.api.post('/api/resource/aclUser', 403, this.user1.token, {
			acl_id: this.user1.publicAcl.id,
			user_id: this.user1.id
		});
	});

	it.skip('Should get users permissions', async function () {
		try {
			const user = this.fixtures.get('user', 0);
			const permissions = await this.api.get('/api/acl', 200, user);
			//console.log(permissions);
		} catch (err) {
			console.log(err);
		}
	});

	it('Should not be possible to create entity with other\'s user acl', async function () {
		const inserted = await this.api.post('/api/resource/testEntity', 403, this.user1.token, {
			title: 'ggg',
			acl_id: this.user2.publicAcl.id
		});
	});

	it('Should be possible to create entity with own public acl', async function () {
		const inserted = await this.api.post('/api/resource/testEntity', 200, this.user1.token, {
			title: 'ggg',
			acl_id: this.user1.publicAcl.id
		});

		await this.api.get('/api/resource/testEntity/' + inserted.id, 200, this.user2.token);
	});	

	it('Should not be possible to insert new acl while creating entity', async function () {
		const inserted = await this.api.post('/api/resource/testEntity', 403, this.user1.token, {
			title: 'ggg',
			acl: {
				title: 'aclTest'
			}
		});
	});

	it.skip('Should not be possible to create user inserted to aclUser resource directly', async function () {
		const inserted = await this.api.post('/api/resource/user', 403, this.user1, {
			first_name: 'test',
			last_name: 'test',
			email: 'ggg',
			aclUser: {
				acl_id: this.loggedUser1.acl_id
			}
		});
	});

	it('Should be possible to change acl on own entity', async function () {
		await this.api.get(`/api/resource/testEntity/${this.inserted.id}`, 404, this.user2.token);
		const updated = await this.api.put(`/api/resource/testEntity/${this.inserted.id}`, 200, this.user1.token, {
			acl_id: this.user1.publicAcl.id
		});
		await this.api.get(`/api/resource/testEntity/${this.inserted.id}`, 200, this.user2.token);
	});

	it('Should not be possible to update entity to have another\'s user acl', async function () {
		const updated = await this.api.put(`/api/resource/testEntity/${this.inserted.id}`, 403, this.user1.token, {
			acl_id: this.user2.publicAcl.id
		});
	});
});