const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const getApiTestUtil = require('../apiTestUtil').getApiTestUtil;
const appFactory = require('../../lib/app-factory');
const getTestConfig = require('../../test-app/config/test.config');
const dbTestConnection = require('../dbTestConnection');

function testEntity (user, expected = {}) {
	beforeEach(function () {
		this.expected = expected;
		this.user = this.users[user];

	});

	if (typeof expected['read'] !== 'undefined') {
		it('Should ' + (expected['read'] ? '' : 'not ') + 'read the entity', async function () {
			const entity = await this.api.get('/api/resource/testEntity/' + this.entityId, this.expected['read'] ? 200 : 404, this.user.token);
			if (this.expected['read']) {
				assert.equal(entity.id, this.entityId);
			}
		});
	}

	if (typeof expected['list'] !== 'undefined') {
		it('Should ' + (expected['list'] ? '' : 'not ') + 'list the entity', async function () {
			const entities = await this.api.get('/api/resource/testEntity', 200, this.user.token);
			const entity = entities.find(e => e.id === this.entityId);
			//assert
		});
	}

	if (typeof expected['update'] !== 'undefined') {
		it('Should ' + (expected['update'] ? '' : 'not ') + 'update the entity', async function () {
			const entity = await this.api.put('/api/resource/testEntity/' + this.entityId, this.expected['update'] ? 200 : 404, this.user.token, {
				title: 'updated'
			});

			if (this.expected['update']) {
				assert.equal(entity.title, 'updated');
			}
		});
	}

	if (typeof expected['delete'] !== 'undefined') {
		it('Should ' + (expected['delete'] ? '' : 'not ') + 'delete the entity', async function () {
			const entity = await this.api.delete('/api/resource/testEntity/' + this.entityId, this.expected['delete'] ? 200 : 404, this.user.token);
		});
	}
}

function testByAllUsers (read, write) {
	
	if (!(read === 'member' && write === 'owner')) {
		//return;
	}

	describe('Test entity which can be read by ' + read + ' and written by ' + write, () => {
		beforeEach(async function () {
			this.users = {
				owner: this.user1,
				member: this.user2,
				nonMember: this.user3
			};
			
			this.entityId = this.inserted.id;

			await this.api.put('/api/acl/' + this.acl.id, 200, this.users.owner.token, { read, write });
		});

		describe('Test as owner', () => {
			testEntity('owner', {
				read: true,
				list: true,
				update: true,
				delete: true
			});
		});

		describe('Test as member', () => {
			testEntity('member', {
				read: ['member', 'public'].includes(read),
				list: ['member', 'public'].includes(read),
				update: ['member', 'public'].includes(write) && ['member', 'public'].includes(read),
				delete: ['member', 'public'].includes(write) && ['member', 'public'].includes(read)
			})
		});

		describe('Test as non-member', () => {
			testEntity('nonMember', {
				read: read === 'public',
				list: read === 'public',
				update: read === 'public' && write === 'public',
				delete: read === 'public' && write === 'public'
			});
		});
	});
}

describe.skip('Resource api with ACL and entity', () => {
	beforeEach(async function () {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);

		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app, testConfig);

		this.user1 = await this.api.login([1]);
		this.user2 = await this.api.login([1]);
		this.user3 = await this.api.login([1]);

		this.acl = await this.api.post('/api/acl', 200, this.user1.token, {
			title: 'test',
			read: 'owner',
			write: 'owner',
			members: [this.user2.id]
		});

		this.inserted = await this.api.post('/api/resource/testEntity', 200, this.user1.token, {
			title: 'ggg',
			acl_id: this.acl.id
		});
	});

	afterEach (async function () {
		await dbTestConnection.stop(this);
	});

	const read = ['owner', 'member', 'public'];
	const write = ['owner', 'member', 'public'];

	for (let r of read) {
		for (let w of write) {
			testByAllUsers(r, w);
		}
	}
});