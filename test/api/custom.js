const chai = require('chai');
const assert = chai.assert;
const R = require('ramda');
const getApiTestUtil = require('../apiTestUtil').getApiTestUtil;
const appFactory = require('../../lib/app-factory');
const getTestConfig = require('../../test-app/config/test.config');
const dbTestConnection = require('../dbTestConnection');

describe('Custom api routes', () => {

	beforeEach(async function () {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);
		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app);
	});

	afterEach (async function () {
		await dbTestConnection.stop(this);
	});

	it('should have services available', async function () {
		const services = await this.api.get('/api/custom/services', 200);
		assert.include(services, 'getResource');
	});
});