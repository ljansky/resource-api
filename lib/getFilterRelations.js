const R = require('ramda');

const isArray = val => R.type(val) === 'Array';

const getFilterRelations = (filters) => {
	return R.reduce((relations, filter) => {
		if (filter.field) {
			const fieldPath = R.split('.', filter.field);
			if (fieldPath.length > 1) {
				const foundIndex = R.findIndex(rel => rel.name === fieldPath[0], relations);
				if (foundIndex === -1) {
					return R.concat(relations, [{ name: fieldPath[0], relations: [] }]);
				} else {
					// TODO - merge relations here?
				}
			}

			if (isArray(filter.value) && filter.operator !== 'in') {
				const foundIndex = R.findIndex(rel => rel.name === filter.field, relations);
				if (foundIndex === -1) {
					const deepRelations = getFilterRelations(filter.value);
					return R.concat(relations, [{ name: filter.field, relations: deepRelations }]);
				} else {
					const deepRelations = getFilterRelations(filter.value);
					const mergedRelations = R.concat(relations[foundIndex].relations, deepRelations);
					const merged = R.assoc('relations', mergedRelations, relations[foundIndex]);
					return R.update(foundIndex, merged, relations);
				}
			}
		} else {
			if (isArray(filter.value) && filter.operator !== 'in') {
				return R.concat(relations, getFilterRelations(filter.value));
			}
		}

		return relations;
	}, [])(filters);
}

module.exports = getFilterRelations;