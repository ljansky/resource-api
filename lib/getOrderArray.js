const R = require('ramda');

module.exports = inputString => {
	return R.pipe(
		R.split(','),
		R.map(itemString => {
			const desc = itemString.charAt(0) === '-';
			const by = desc ? R.slice(1, Infinity, itemString) : itemString;
			return { by, desc };
		})
	)(inputString);
};
