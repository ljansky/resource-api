const R = require('ramda');
const parseFunction = require('entity-formatter').parseFunction;
const getPrefixArray = require('./getPrefixArray');

const getFilterByFunction = (parsedFunction, field) => {
	switch(parsedFunction.name) {
		case 'like':
		case '_like':
		case 'like_':
		case 'gte':
		case 'lt':
		case 'lte':
			return {
				field,
				operator: parsedFunction.name,
				value: parsedFunction.params[0]
			}
		default:
			return null;
	}
}

module.exports = R.pipe(
	getPrefixArray('f'),
	R.map(item => {
		const parsed = parseFunction(item.value);
		return parsed.params.length ? getFilterByFunction(parsed, item.key) : {
			field: item.key,
			operator: 'eq',
			value: item.value
		}
	})
);