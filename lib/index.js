const appFactory = require('./app-factory');
const getResourceFactory = require('./resource').getResourceFactory;
const getFilterArray = require('./getFilterArray');
const getOrderArray = require('./getOrderArray');

const getCoreModelsPath = () => {
	return __dirname + '/models';
}

module.exports = {
	appFactory,
	getResourceFactory,
	getCoreModelsPath,
	getFilterArray,
	getOrderArray
};