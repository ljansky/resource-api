const R = require('ramda');

module.exports = R.curry((prefix, object) => {
	return R.pipe(
		R.toPairs,
		R.map(pair => {
			const path = pair[0].split('.');
			return { 
				prefix: path.shift(),
				key: path.join('.'),
				value: pair[1] 
			};
		}),
		R.filter(item => item.prefix === prefix && item.key !== '')
	)(object);
});
