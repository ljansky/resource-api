const R = require('ramda');
const error = require('./error');

const getOperationFilter = (aclUser, write) => R.concat([{
	operator: 'eq',
	field: `${aclUser}_read`,
	value: 1
}], write ? [{
	operator: 'eq',
	field: `${aclUser}_write`,
	value: 1
}] : []);

const getAclQuery = (userId, write) => {

	const ownerQuery = {
		operator: 'and',
		field: 'acl',
		value: [{
			operator: 'eq',
			field: 'user_id',
			value: userId
		}]
	}

	const memberQuery = {
		operator: 'and',
		field: 'acl',
		value: R.append({
			operator: 'has',
			field: 'aclUser',
			value: [{
				operator: 'eq',
				field: 'user_id',
				value: userId
			}]
		}, getOperationFilter('member', write))
	};

	const publicQuery = {
		operator: 'and',
		field: 'acl',
		value: getOperationFilter('public', write)
	};

	//console.log(ownerQuery);
	//console.log(memberQuery);
	//console.log(publicQuery);

	return [{
		operator: 'or',
		value: [ownerQuery, memberQuery, publicQuery]
	}];
};

const getOnInsertData = (getResource, user) => async (data, storage) => {
	let resData = data;

	// set users system acl when created
	if (storage.storageConfig.name === 'user') {
		resData = R.assoc('acl', [{
			title: 'private',
			system: 1
		}, {
			title: 'public',
			system: 1,
			public_read: 1,
			member_read: 1
		}], data);
	}

	// set acl_id to users private acl when creating entity related to acl
	// or allow only own acl if acl_id defined
	if (user && storage.storageConfig.relatedStorages.acl) {
		if (typeof data.acl !== 'undefined') {
			error.forbidden();
		}

		const aclResource = getResource('acl');

		if (typeof data.acl_id !== 'undefined') {
			const acl = await aclResource.findAll({
				filter: [{
					field: 'user_id',
					operator: 'eq',
					value: user.id
				}, {
					field: 'id',
					operator: 'eq',
					value: data.acl_id
				}]
			});

			if (acl.length === 0) {
				error.forbidden();
			}
		} else {
			const acl = await aclResource.findAll({
				filter: [{
					field: 'user_id',
					operator: 'eq',
					value: user.id
				}, {
					field: 'system',
					operator: 'eq',
					value: 1
				}, {
					field: 'title',
					operator: 'eq',
					value: 'private'
				}]
			});

			if (acl[0]) {
				resData = R.assoc('acl_id', acl[0].id, data);
			}
		}
	}

	return resData
}

const getOnUpdateData = (getResource, user) => async (data, storage) => {
	if (user && storage.storageConfig.relatedStorages.acl) {
		if (typeof data.acl !== 'undefined') {
			error.forbidden();
		}

		const aclResource = getResource('acl');

		if (typeof data.acl_id !== 'undefined') {
			const acl = await aclResource.findAll({
				filter: [{
					field: 'user_id',
					operator: 'eq',
					value: user.id
				}, {
					field: 'id',
					operator: 'eq',
					value: data.acl_id
				}]
			});

			if (acl.length === 0) {
				error.forbidden();
			}
		}
	}

	return data;
}

const getAclResource = getResource => user => (resourceName, write = false) => {
	const aclQuery = getAclQuery(user.id, write);

	return getResource(resourceName, aclQuery, {
		onInsertData: getOnInsertData(getResource, user),
		onUpdateData: getOnUpdateData(getResource, user)
	});
}

module.exports = {
	getAclQuery,
	getOnInsertData,
	getAclResource
};