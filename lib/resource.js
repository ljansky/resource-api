const R = require('ramda');
const getFilterRelations = require('./getFilterRelations');

const findAll = (storage, formatter, defaultFilter) => async (params = {}) => {
	const format = params.format || {};
	const filter = R.concat(defaultFilter, params.filter || []);
	const limit = params.limit || 0;
	const offset = params.offset || 0;
	const order = params.order || [];

	const formatObject = typeof format === 'string' ? formatter.parse(format) : format;
	const filterRelations = getFilterRelations(filter);
	const relations = R.concat(formatter.getRelations(formatObject), filterRelations);

	const entities = await storage.find({ filter, relations, limit, offset, order });
	const formatOne = formatter.formatOne(formatObject);
	return R.map(formatOne, entities);
};

const findOneById = (storage, formatter, defaultFilter) => async (id, format = '*') => {

	const filter = [{
		field: 'id',
		operator: 'eq',
		value: id
	}];

	const entities = await findAll(storage, formatter, defaultFilter)({ filter, format });

	return entities[0] || null;
};

const insertOne = (storage, onInsertData) => async (data) => {
	let insertData = data;
	if (onInsertData) {
		insertData = await onInsertData(data, storage);
	}

	return storage.insertOne(insertData);
};

const updateOneById = (storage, onUpdateData) => async (id, data) => {
	let updateData = data;
	if (onUpdateData) {
		updateData = await onUpdateData(data, storage);
	}

	return storage.updateOneById(id, updateData);
};

const deleteOneById = storage => id => {
	return storage.deleteOneById(id);
};

const getResourceFactory = (getStorage, formatter) => (name, defaultFilter = [], events = {}) => {

	const storage = getStorage(name);
	if (!storage) {
		return null;
	}

	return {
		findAll: findAll(storage, formatter, defaultFilter),
		findOneById: findOneById(storage, formatter, defaultFilter),
		insertOne: insertOne(storage, events.onInsertData),
		updateOneById: updateOneById(storage, events.onUpdateData),
		deleteOneById: deleteOneById(storage)
	};
};

module.exports = {
	getResourceFactory
};