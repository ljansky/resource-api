const getMiddleware = config => {
	return async (ctx, next) => {
		ctx.services = config.services;
		await next();
	};
};

module.exports = {
	getMiddleware
};