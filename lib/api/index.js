const KoaRouter = require('koa-router');
const koaBodyParser = require('koa-bodyparser');
const router = new KoaRouter();
const resource = require('./resource');
const acl = require('./acl');
const auth = require('./auth');
const passport = require('./passport').passport;


const isAuthenticated = async function (ctx, next) {
	await next();
}

module.exports = async function (customRouter) {
	//const configMiddleware = await config.getMiddleware();
	//router.use(configMiddleware);
	
	router.use('/api/auth', auth.routes());

	router.use('/api/resource',
		passport.authenticate('jwt', { session: false }),
		resource.routes()
	);

	router.use('/api/acl',
		passport.authenticate('jwt', { session: false }),
		acl.routes()
	);

	if (customRouter) {
		router.use('/api/custom', koaBodyParser(), customRouter.routes());
	}

	return router.routes();
};
