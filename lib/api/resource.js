const KoaRouter = require('koa-router');
const router = new KoaRouter();
const koaBodyParser = require('koa-bodyparser');
const error = require('../error');
const getFilterArray = require('../getFilterArray');
const getOrderArray = require('../getOrderArray');
const acl = require('../acl');
const R = require('ramda');

const resourceMiddleware = write => async (ctx, next) => {
	const user = ctx.state.user;

	const resourceName = ctx.params.resource;

	ctx.resource = acl.getAclResource(ctx.services.getResource)(user)(resourceName, write);
	if (!ctx.resource) {
		error.notFound('Resource not found');
	}

	const allowed = R.pipe(
		R.map(R.prop('role')),
		R.map(R.prop('roleResource')),
		R.flatten,
		R.any(R.whereEq({ resource: resourceName, [write ? 'write' : 'read']: 1 }))
	)(user.userRole);

	if (!allowed) {
		error.forbidden('Resource forbidden');
	}

	await next();
}

router.get('/:resource', resourceMiddleware(false), async function (ctx) {
	try {
		const format = ctx.query.format;
		const filter = getFilterArray(ctx.query);
		const limit = parseInt(ctx.query.limit);
		const offset = parseInt(ctx.query.offset);
		const order = ctx.query.order ? getOrderArray(ctx.query.order) : [];

		ctx.body = await ctx.resource.findAll({ filter, format, limit, offset, order });
	} catch (err) {
		console.log('ERR', err);
	}
	
});

router.get('/:resource/:id', resourceMiddleware(false), async function (ctx) {
	try {
		const id = ctx.params.id;
		const format = ctx.query.format;

		const found = await ctx.resource.findOneById(id, format);
		if (!found) {
			error.notFound('Item not found');
		}

		ctx.body = found;
	} catch (err) {
		// console.log('ERR', err);
	}
	
});

router.post('/:resource', koaBodyParser(), resourceMiddleware(true), async function (ctx) {
	const inserted = await ctx.resource.insertOne(ctx.request.body);
	ctx.body = inserted;
});

router.put('/:resource/:id', koaBodyParser(), resourceMiddleware(true), async function (ctx) {
	const id = ctx.params.id;

	const found = await ctx.resource.findOneById(id);

	if (!found) {
		error.notFound('Item not found');
	}

	const updated = await ctx.resource.updateOneById(id, ctx.request.body);

	if (!updated) {
		error.notFound('Item not found');
	}

	ctx.body = updated;
});

router.delete('/:resource/:id', resourceMiddleware(true), async function (ctx) {
	const id = ctx.params.id;

	const found = await ctx.resource.findOneById(id);
	if (!found) {
		error.notFound('Item not found');
	}

	const deleted = await ctx.resource.deleteOneById(id);

	if (!deleted) {
		error.notFound('Item not found');
	}

	ctx.body = deleted;
});

module.exports = router;
