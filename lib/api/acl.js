const KoaRouter = require('koa-router');
const router = new KoaRouter();
const koaBodyParser = require('koa-bodyparser');
const error = require('../error');
const R = require('ramda');

router.get('/', async (ctx, next) => {
	const user = ctx.state.user;
	const aclResource = ctx.services.getResource('acl');
	const filter = [{
		field: 'user_id',
		operator: 'eq',
		value: user.id
	}];

	const items = await aclResource.findAll({ filter });
	ctx.body = items;
});

router.get('/:id', async (ctx, next) => {
	const user = ctx.state.user;
	const id = parseInt(ctx.params.id);
	const aclResource = ctx.services.getResource('acl');
	const filter = [{
		field: 'user_id',
		operator: 'eq',
		value: user.id
	}, {
		field: 'id',
		operator: 'eq',
		value: id
	}];

	const items = await aclResource.findAll({ filter });
	ctx.body = items[0] || null;
});

router.post('/', koaBodyParser(), async (ctx) => {

	const user = ctx.state.user;
	const body = ctx.request.body;

	const acl = {
		title: body.title,
		member_read: body.read === 'member' || body.read === 'public',
		member_write: body.write === 'member' || body.write === 'public',
		public_read: body.read === 'public',
		public_write: body.write === 'public',
		user_id: user.id
	};

	if (body.members) {
		acl.aclUser = body.members.map(userId => ({ user_id: userId }));
	}
	
	const aclResource = ctx.services.getResource('acl');
	ctx.body = await aclResource.insertOne(acl);
});

router.put('/:id', koaBodyParser(), async (ctx) => {
	
	const user = ctx.state.user;
	const id = parseInt(ctx.params.id);

	const ownAcl = R.any(R.propEq('id', id), user.acl);

	if (!ownAcl) {
		error.forbidden();
	}

	const body = ctx.request.body;

	const acl = {};
	if (body.title) {
		acl.title = body.title;
	}

	if (body.read) {
		acl.member_read = body.read === 'member' || body.read === 'public';
		acl.public_read = body.read === 'public';
	}

	if (body.write) {
		acl.member_write = body.write === 'member' || body.write === 'public';
		acl.public_write = body.write === 'public';
	}

	const aclResource = ctx.services.getResource('acl');
	const item = await aclResource.updateOneById(ctx.params.id, acl);

	if (!item) {
		error.forbidden();
	}

	ctx.body = item;
});

module.exports = router;