const passport = require('koa-passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const LocalStrategy  = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;

const JWT_SECRET = 'totoJeJWTSecret';

const configure = services => {

	const userResource = services.getResource('user');
	// TODO: Hash the password !!
	passport.use(new LocalStrategy({
			usernameField: 'email',
			passwordField: 'password',
			session: false
		},
		async (email, password, cb) => {
			const filter = [{
				field: 'email',
				operator: 'eq',
				value: email
			}, {
				field: 'loginPassword',
				operator: 'has',
				value: [{
					field: 'password',
					operator: 'eq',
					value: password
				}]
			}];

			const format = 'id,email,userRole.role.roleMenu.menu.*'

			const users = await userResource.findAll({ filter, format });

			return cb(null, users[0] || false);
		}
	));

	passport.use(new JWTStrategy({
			jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
			secretOrKey   : JWT_SECRET
		},
		async (jwtPayload, cb) => {
			const user = await userResource.findOneById(jwtPayload.userId, 'id,email,(userRole,(role,(roleResource),(roleMenu,(menu)))),(acl)');
			return cb(null, user);		
		}
	));
/*
	passport.use(new FacebookStrategy({
			clientID: '235788693661818',
			clientSecret: 'ff27561358295e9e413d8b66c1d3bf48',
			callbackURL: "http://localhost:3000/auth/facebook/callback"
		},
		(accessToken, refreshToken, profile, cb) => {
			const user = {
				fb: 'asdf'
			};

			return cb(err, user);
		}
	));*/
}

module.exports = {
	configure,
	passport,
	JWT_SECRET
}