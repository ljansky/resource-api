const KoaRouter = require('koa-router');
const router = new KoaRouter();
const koaBodyParser = require('koa-bodyparser');
const error = require('../error');
const passport = require('./passport').passport;
const JWT_SECRET = require('./passport').JWT_SECRET;
const jwt = require('jsonwebtoken');
const R = require('ramda');

const getUserMenu = R.pipe(
  R.prop('userRole'),
  R.map(userRole => userRole.role.roleMenu),
  R.reduce(R.concat, []),
  R.pluck('menu'),
  R.uniq
)

router.get('/login', (ctx, next) => {
	return passport.authenticate('jwt', { session: false }, async (err, user, info) => {
    if (err || !user) {
      ctx.body = {}
    } else {
      ctx.body = {
        id: user.id,
        menu: getUserMenu(user)
      }
    }
  })(ctx, next);
});

router.post('/login', (ctx, next) => {
  return passport.authenticate('local', async (err, user, info) => {
    if (err || !user) {
      ctx.status = 401;
      ctx.body = { error: 1 }
    } else {
      const token = jwt.sign({ userId: user.id }, JWT_SECRET);
      ctx.body = {
        token,
        info: {
          id: user.id,
          menu: getUserMenu(user)
        }
      }
    }
  })(ctx, next);
});

module.exports = router;