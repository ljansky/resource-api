'use strict';

class AppError {
	constructor(code, message, data) {
		this.message = message;
		this.status = code;
		this.data = data || null;
	}
}

const error = {

	getError: function(code, message, data) {
		return new AppError(code, message, data);
	},

	throwError: function(code, message, data) {
		throw this.getError(code, message, data);
	},

	notFound: function(message, data) {
		message = message || 'Not Found';
		this.throwError(404, message, data);
	},

	forbidden: function(message, data) {
		message = message || 'Forbidden';
		this.throwError(403, message, data);
	},

	unauthorized: function(message, data) {
		message = message || 'Unauthorized';
		this.throwError(401, message, data);
	},

	badRequest: function(message, data) {
		message = message || 'Bad request';
		this.throwError(400, message, data);
	}
};

module.exports = error;