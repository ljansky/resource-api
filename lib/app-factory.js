const Koa = require('koa');
const getRoutes = require('./api');
const errorHandler = require('./api/errorHandler');
const cors = require('koa-cors');
const getServicesMiddleware = require('./services-factory').getMiddleware;
const passportConfigure = require('./api/passport').configure;
const passport = require('./api/passport').passport;
const bodyParser = require('koa-bodyparser');

let app = null;

module.exports = async function (appConfig) {
	if (!app) {
		app = new Koa();

		//todo: toto dat pak pryc
		app.use(cors({
			origin: '*'
		}));
		
		app.use(bodyParser());
		app.use(passport.initialize());
		passportConfigure(appConfig.services);

		app.use(getServicesMiddleware(appConfig));
		app.use(errorHandler);

		const routes = await getRoutes(appConfig.api);

		app.use(routes);
	}

	return app;
};